package com.timteamdev.currencyprices.ui.chart

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.timteamdev.currencyprices.model.ChartItem
import com.timteamdev.currencyprices.repository.ChartDataRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChartViewModel(private var chartDataRepository: ChartDataRepository) : ViewModel() {

    var chartItems = MutableLiveData<ArrayList<ChartItem>>()

    fun getData(currencyPairName: String) {
        chartDataRepository
            .getPrice(currencyPairName)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->

                if (chartItems.value == null) {
                    chartItems.value = arrayListOf()
                }

                var items = arrayListOf<ChartItem>()
                items.addAll(chartItems.value!!)
                items.add(result)
                chartItems.value = items

            }, { error ->
                error.printStackTrace()
            }, { })
    }

}
