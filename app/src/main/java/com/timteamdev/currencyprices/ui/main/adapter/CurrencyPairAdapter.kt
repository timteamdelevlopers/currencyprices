package com.timteamdev.currencyprices.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.timteamdev.currencyprices.databinding.ItemCurrencyPairBinding
import com.timteamdev.currencyprices.model.CurrencyPair

class CurrencyPairAdapter(
    private var items: ArrayList<CurrencyPair>,
    private var listener: OnItemClickListener
) : RecyclerView.Adapter<CurrencyPairViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CurrencyPairViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCurrencyPairBinding.inflate(layoutInflater, parent, false)
        return CurrencyPairViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CurrencyPairViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    fun setData(arrayList: ArrayList<CurrencyPair>) {
        items.clear()
        items.addAll(arrayList)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(pairName: String)
    }

}