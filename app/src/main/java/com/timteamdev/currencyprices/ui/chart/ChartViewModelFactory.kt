package com.timteamdev.currencyprices.ui.chart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.timteamdev.currencyprices.repository.ChartDataRepository

class ChartViewModelFactory(private val chartDataRepository: ChartDataRepository)
    : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChartViewModel::class.java)) {
            return ChartViewModel(chartDataRepository) as T
        }

        throw IllegalArgumentException("Unknown ViewModel Class")
    }

}