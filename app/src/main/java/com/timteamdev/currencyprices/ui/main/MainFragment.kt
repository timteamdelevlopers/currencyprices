package com.timteamdev.currencyprices.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.timteamdev.currencyprices.R
import com.timteamdev.currencyprices.databinding.MainFragmentBinding
import com.timteamdev.currencyprices.di.Injection
import com.timteamdev.currencyprices.model.CurrencyPair
import com.timteamdev.currencyprices.ui.chart.ChartFragment
import com.timteamdev.currencyprices.ui.main.adapter.CurrencyPairAdapter

class MainFragment : Fragment(), CurrencyPairAdapter.OnItemClickListener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    private lateinit var binding: MainFragmentBinding
    private val currencyPairAdapter = CurrencyPairAdapter(arrayListOf(), this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModelFactory = Injection.provideMainViewModelFactory()
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        binding.viewModel = viewModel
        binding.rvPairs.layoutManager = LinearLayoutManager(context)
        binding.rvPairs.adapter = currencyPairAdapter
        viewModel.pairs.observe(this,
            Observer<ArrayList<CurrencyPair>> { it?.let { currencyPairAdapter.setData(it) } })
    }

    override fun onItemClick(pairName: String) {
        val action = MainFragmentDirections.actionMainFragmentToChartFragment(pairName)
        findNavController().navigate(action)
    }

}
