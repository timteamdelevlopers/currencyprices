package com.timteamdev.currencyprices.ui.chart

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.ValueFormatter

import com.timteamdev.currencyprices.R
import com.timteamdev.currencyprices.databinding.ChartFragmentBinding
import com.timteamdev.currencyprices.di.Injection
import com.timteamdev.currencyprices.model.ChartItem
import java.util.ArrayList

class ChartFragment : Fragment() {

    companion object {
        fun newInstance() = ChartFragment()
    }

    private lateinit var viewModel: ChartViewModel

    private lateinit var binding: ChartFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.chart_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModelFactory = Injection.provideChartViewModelFactory()
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ChartViewModel::class.java)

        viewModel.chartItems.observe(this,
            Observer<ArrayList<ChartItem>> { it?.let { updateData(it) } })

        settingChart(binding.lineChart)

        val pairName = ChartFragmentArgs.fromBundle(arguments!!).pair

        binding.tvTitle.text = pairName
        viewModel.getData(pairName)

    }

    private fun settingChart(chart: LineChart) {

        chart.setPinchZoom(true)
        chart.description.isEnabled = false
        chart.axisLeft.isEnabled = false
        chart.legend.isEnabled = false
        chart.extraLeftOffset = 20f

//        val mv = BubbleMarkerView(this, R.layout.view_marker, this)
//        mv.setChartView(mChart)
//        mChart.setMarker(mv)

        val xAxis = chart.xAxis
        xAxis.textSize = 10f
        xAxis.textColor = ResourcesCompat.getColor(resources, R.color.chart_axis_text, null)
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.isGranularityEnabled = true
        xAxis.gridColor = ResourcesCompat.getColor(resources, R.color.chart_axis_grid, null)
        xAxis.gridLineWidth = 1f
        xAxis.setDrawAxisLine(false)
        xAxis.labelCount = 4
        xAxis.valueFormatter = object : ValueFormatter() {

            override fun getFormattedValue(value: Float): String {
                return ""
//                return if (value > 0
//                    && mChartItems != null
//                    && mChartItems.size > value.toInt()
//                ) {
//                    mChartItems.get(value.toInt()).getFormattedTime()
//                } else ""

            }

        }

        val rightAxis = chart.axisRight
        rightAxis.textSize = 10f
        rightAxis.textColor = ResourcesCompat.getColor(resources, R.color.chart_axis_text, null)
        rightAxis.setDrawGridLines(true)
        rightAxis.setDrawAxisLine(false)
        rightAxis.isGranularityEnabled = true
        rightAxis.gridColor = ResourcesCompat.getColor(resources, R.color.chart_axis_grid, null)
        rightAxis.gridLineWidth = 1f
        rightAxis.granularity = 0.00000001f

        chart.animateX(900)
    }

    private fun updateData(chartItems: List<ChartItem>) {

        val dataSet: LineDataSet

        val values = ArrayList<Entry>()

        for (item in chartItems) {

            values.add(Entry(values.size.toFloat(), item.value.toFloat()))

        }

        var currentChartData = binding.lineChart.data

        if (currentChartData != null && currentChartData.dataSetCount > 0) {

            dataSet = currentChartData.getDataSetByIndex(0) as LineDataSet
            dataSet.values = values
            dataSet.notifyDataSetChanged()
            currentChartData.notifyDataChanged()
            binding.lineChart.notifyDataSetChanged()

        } else {

            dataSet = LineDataSet(values, "entries")
            dataSet.axisDependency = YAxis.AxisDependency.RIGHT
            dataSet.color = ResourcesCompat.getColor(resources, R.color.green, null)
            dataSet.lineWidth = 2f
            dataSet.setDrawCircleHole(false)
            dataSet.setDrawCircles(false)
            dataSet.setDrawHorizontalHighlightIndicator(true)
            dataSet.setDrawVerticalHighlightIndicator(true)

            dataSet.setDrawFilled(true)
            dataSet.fillFormatter =
                IFillFormatter { _, _ ->
                    binding.lineChart.axisLeft.axisMinimum
                }

            dataSet.highlightLineWidth = 1f

            val drawable = ResourcesCompat.getDrawable(resources, R.drawable.bg_chart, null)
            dataSet.fillDrawable = drawable

            val data = LineData(dataSet)
            data.setDrawValues(false)

            binding.lineChart.data = data
        }

        binding.lineChart.invalidate()
    }

}
