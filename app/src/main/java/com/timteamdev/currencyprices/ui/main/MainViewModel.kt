package com.timteamdev.currencyprices.ui.main

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.timteamdev.currencyprices.model.CurrencyPair
import com.timteamdev.currencyprices.repository.AppRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewModel(private var appRepository: AppRepository) : ViewModel() {

    var pairs = MutableLiveData<ArrayList<CurrencyPair>>()
    var isLoading = ObservableBoolean(false)

    private fun loadCurrencyPairs() {
        isLoading.set(true)
        appRepository
            .getCurrencyPairs()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                pairs.value = result
            }, { error ->
                error.printStackTrace()
            }, { isLoading.set(false) })
    }

    fun onSwipeRefresh() {
        loadCurrencyPairs()
    }

}
