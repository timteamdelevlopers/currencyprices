package com.timteamdev.currencyprices.ui.main.adapter

import androidx.recyclerview.widget.RecyclerView
import com.timteamdev.currencyprices.databinding.ItemCurrencyPairBinding
import com.timteamdev.currencyprices.model.CurrencyPair

class CurrencyPairViewHolder(private var binding: ItemCurrencyPairBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: CurrencyPair, listener: CurrencyPairAdapter.OnItemClickListener?) {
        binding.currencyPair = item
        if (listener != null) {
            binding.root.setOnClickListener { _ -> listener.onItemClick(item.name) }
        }

        binding.executePendingBindings()
    }

}