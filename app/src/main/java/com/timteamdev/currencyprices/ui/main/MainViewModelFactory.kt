package com.timteamdev.currencyprices.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.timteamdev.currencyprices.repository.AppRepository

class MainViewModelFactory(private val appRepository: AppRepository)
    : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(appRepository) as T
        }

        throw IllegalArgumentException("Unknown ViewModel Class")
    }

}