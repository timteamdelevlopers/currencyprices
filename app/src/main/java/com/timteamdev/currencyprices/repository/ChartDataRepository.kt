package com.timteamdev.currencyprices.repository

import com.timteamdev.currencyprices.model.ChartItem
import com.timteamdev.currencyprices.socket.SocketManager
import io.reactivex.Observable

class ChartDataRepository(private val socketManager: SocketManager) {

    fun getPrice(currencyPairName: String): Observable<ChartItem> {

        return socketManager.subscribe(currencyPairName)
    }


}