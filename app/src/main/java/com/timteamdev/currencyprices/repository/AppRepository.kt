package com.timteamdev.currencyprices.repository

import com.timteamdev.currencyprices.api.ApiService
import com.timteamdev.currencyprices.model.CurrencyPair
import io.reactivex.Observable

class AppRepository(private val apiService: ApiService) {

    fun getCurrencyPairs(): Observable<ArrayList<CurrencyPair>> {

        return apiService.getCurrencyPairs()
            .flatMap { result ->

                var pairsArrayList = ArrayList<CurrencyPair>()

                result.forEach {
                    pairsArrayList.add(CurrencyPair(it))
                }

                return@flatMap Observable.just(pairsArrayList)
            }
    }

}