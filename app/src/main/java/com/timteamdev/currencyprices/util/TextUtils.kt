package com.timteamdev.currencyprices.util

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

object TextUtils {

    private const val FORMAT_DOUBLE = "dd.MM.yyyy HH:mm:ss"

    fun getFormattedDouble(value: Double): String {
        return getGroupFormat(FORMAT_DOUBLE).format(value)
    }

    private fun getGroupFormat(format: String): DecimalFormat {
        val formatSymbols = DecimalFormatSymbols(Locale.getDefault())
        formatSymbols.decimalSeparator = '.'
        formatSymbols.groupingSeparator = ' '
        return DecimalFormat(format, formatSymbols)
    }

}