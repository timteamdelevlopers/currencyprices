package com.timteamdev.currencyprices.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    private const val FORMAT_FULL_DATE = "dd.MM.yyyy HH:mm:ss"
    private const val FORMAT_TIME = "HH:mm:ss"

    fun getFullFormattedDate(timeInMillis: Long): String {
        return getFormattedDate(timeInMillis, FORMAT_FULL_DATE)
    }

    fun getTimeFormattedDate(timeInMillis: Long): String {
        return getFormattedDate(timeInMillis, FORMAT_TIME)
    }

    private fun getFormattedDate(timeInMillis: Long, dateFormat: String): String {
        val simpleDateFormat = SimpleDateFormat(dateFormat, Locale.getDefault())

        val date = Date(timeInMillis)

        return simpleDateFormat.format(date)
    }

}