package com.timteamdev.currencyprices.di

import com.timteamdev.currencyprices.api.ApiService
import com.timteamdev.currencyprices.repository.AppRepository
import com.timteamdev.currencyprices.repository.ChartDataRepository
import com.timteamdev.currencyprices.socket.SocketManager
import com.timteamdev.currencyprices.ui.chart.ChartViewModelFactory
import com.timteamdev.currencyprices.ui.main.MainViewModelFactory

object Injection {

    private var APP_REPOSITORY: AppRepository? = null
    private var CHART_DATA_REPOSITORY: ChartDataRepository? = null

    private fun provideAppRepository() : AppRepository {
        if(APP_REPOSITORY == null) {
            APP_REPOSITORY = AppRepository(ApiService.create())
        }
        return APP_REPOSITORY!!
    }

    fun provideMainViewModelFactory() : MainViewModelFactory {
        val appRepository = provideAppRepository()
        return MainViewModelFactory(appRepository)
    }

    private fun provideChartDataRepository() : ChartDataRepository {
        if(CHART_DATA_REPOSITORY == null) {
            CHART_DATA_REPOSITORY = ChartDataRepository(SocketManager.create())
        }
        return CHART_DATA_REPOSITORY!!
    }

    fun provideChartViewModelFactory() : ChartViewModelFactory {
        val chartDataRepository = provideChartDataRepository()
        return ChartViewModelFactory(chartDataRepository)
    }

}