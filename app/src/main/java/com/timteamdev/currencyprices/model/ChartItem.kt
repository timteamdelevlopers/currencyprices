package com.timteamdev.currencyprices.model

class ChartItem(
    var time: Long,
    var value: Double,
    var formattedTime: String?
)