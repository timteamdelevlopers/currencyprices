package com.timteamdev.currencyprices.socket

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.timteamdev.currencyprices.model.ChartItem
import io.reactivex.subjects.ReplaySubject
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.lang.Exception
import java.net.URI
import java.util.*


class SocketManager() {

    private val SUBSCRIBE = "subscribe"
    private val UNSUBSCRIBE = "unsubscribe"
    private val SUBSCRIBED = "subscribed"
    private val UNSUBSCRIBED = "unsubscribed"
    private val INFO = "info"
    private val TICKER = "ticker"
    private val OK = "ok"

    private var socket: WebSocketClient? = null
    var connected = false
    private var lastChannelId: Int = 0
    private var lastNotSendingMessage: String? = null

    private var source: ReplaySubject<ChartItem> = ReplaySubject.create()

    fun open() {

        if (socket == null) {

            socket = object : WebSocketClient(URI.create("wss://api.bitfinex.com/ws/")) {

                override fun onOpen(handshakedata: ServerHandshake?) {
                    Log.d("SocketManager", "onOpen")
                    connected = true
                    reSendLastNotSendingMessage()

                }

                override fun onClose(code: Int, reason: String?, remote: Boolean) {

                    Log.d("SocketManager", "onClose")
                    connected = false
                }

                override fun onMessage(message: String?) {

                    Log.d("SocketManager", "onMessage " + message)

                    if (message.isNullOrEmpty()) {
                        return
                    }

                    val time = Calendar.getInstance().timeInMillis

                    if (message[0] == '[') {
                        try {
                            val sType = object : TypeToken<List<Double>>() {}.type
                            val values = Gson().fromJson<List<Double>>(message, sType)

                            val chartItem = ChartItem(time, values[7], null)

                            source.onNext(chartItem)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else {

                        val socketMessage = Gson().fromJson(message, SocketMessage::class.java)
                        if (socketMessage.event == SUBSCRIBED) {
                            lastChannelId = socketMessage.chanId!!
                        }
                    }

                }

                override fun onError(ex: Exception?) {

                    Log.d("SocketManager", "onError" + ex!!.message)

                    if (ex != null) {
                        source.onError(ex)
                    }
                }
            }
           // socket?.connect()
        }
    }

    fun close() {
        socket?.close()
    }

    fun subscribe(currencyPairName: String): ReplaySubject<ChartItem> {

        if (lastChannelId > 0) {
            unsubscribe()
        }

        source = ReplaySubject.create()

        val message = SocketMessage(SUBSCRIBE, TICKER, currencyPairName, null, null)
        val jsonMessage = Gson().toJson(message)

        socketSendMessage(jsonMessage)

        return source
    }

    fun unsubscribe() {
        unsubscribe(lastChannelId)
        lastChannelId = 0;
    }

    private fun unsubscribe(channelId: Int) {
        val message = SocketMessage(UNSUBSCRIBE, null, null, channelId, null)
        val jsonMessage = Gson().toJson(message)

        socketSendMessage(jsonMessage)
    }

    private fun socketSendMessage(message: String) {
        if (socket != null
            && socket!!.isOpen) {
            socket?.send(message)
        } else {
            lastNotSendingMessage = message
            socket?.connect()
        }
    }

    private fun reSendLastNotSendingMessage() {
        if(lastNotSendingMessage != null) {
            socketSendMessage(lastNotSendingMessage!!)
            lastNotSendingMessage = null
        }
    }

    companion object Factory {
        fun create(): SocketManager {
            val socketManager = SocketManager()
            socketManager.open()
            return socketManager
        }
    }
}