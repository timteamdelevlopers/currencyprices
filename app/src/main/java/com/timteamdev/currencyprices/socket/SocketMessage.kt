package com.timteamdev.currencyprices.socket

data class SocketMessage(
    var event: String,
    var channel: String?,
    var pair: String?,
    var chanId: Int?,
    var status: String?
)